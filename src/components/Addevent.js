import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

class AddEvent extends Component {
    addEvent(newevent){
        axios.request({
            method:'post',
            url:'https://loopback-crud1.herokuapp.com/api/events',
            data:newevent
        })
        .then(response=>{
            this.props.history.push('/');
        })
        .catch(err=>console.log(err));
    }
  onSubmit(e) {
      const newevent={
     name:this.refs.name.value,
     city:this.refs.city.value,
     address:this.refs.address.value
      }
      this.addEvent(newevent)
    e.preventDefault();
  }
  render() {
    return (
      <div>
          <br />
        <Link className="btn grey" to="/">
          Back
        </Link>
        <h1>Add Event</h1>
        <form onSubmit={this.onSubmit.bind(this)}>
          <div className="input-field">
            <input type="text" name="name" ref="name" />
            <label htmlFor="name">Name</label>
          </div>
          <div className="input-field">
            <input type="text" name="city" ref="city" />
            <label htmlFor="city">City</label>
          </div>
          <div className="input-field">
            <input type="text" name="address" ref="address" />
            <label htmlFor="Address">Address</label>
          </div>
          <input type="submit" value="Save" className="btn" />
        </form>
      </div>
    );
  }
}
export default AddEvent;
