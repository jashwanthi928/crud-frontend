import React from "react";
import { Switch, Route } from "react-router-dom";
import Events from "./Events";
import About from "./About";
import EventDetails from "./Eventdetails";
import AddEvent from './Addevent'
import EditEvent from './Editevent'

const Main = () => (
  <main>
    <Switch>
      <Route exact path="/" component={Events} />
      <Route exact path="/about" component={About} />
      <Route exact path="/events/add" component={AddEvent}/>
      <Route exact path="/events/edit/:id" component={EditEvent}/>
      <Route exact path="/events/:id" component={EventDetails}/>
    </Switch>
  </main>
);
export default Main;
