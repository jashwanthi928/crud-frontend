import React, {Component} from 'react';
import axios from 'axios';
import EventItem from './Eventitem';

class Events extends Component{
    constructor(){
        super();
        this.state={
            events:[]
        }
    }
    componentWillMount(){
        this.getEvents();
    }
    getEvents(){
        axios.get('https://loopback-crud1.herokuapp.com/api/events')
        .then(response=>{
           this.setState({events:response.data},()=>{
               //console.log(this.state);
           });
        })
        .catch(err=>console.log(err));
    }
    render(){
        const events=this.state.events.map((event,i)=>{
            return(
                <EventItem key={event.id} item={event}/>
            )
        })
        return(
            <div>
            <h1>Events</h1>
            <ul className="collection">
               {events} 
            </ul>
            </div>
        )
    }
}
export default Events;